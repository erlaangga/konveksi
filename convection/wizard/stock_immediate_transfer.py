from odoo import models, api

class StockImmediateTransfer(models.TransientModel):
    _inherit = 'stock.immediate.transfer'

    def process(self):
        for pick in self.pick_ids:
            if len(pick.move_group_line) == 1:
                pick.move_group_line.qty_done = pick.move_group_line.qty_detail1
        res = super(StockImmediateTransfer, self).process()
        for pick in self.pick_ids:
            for line in pick.move_group_line:
                line.state = "done"
        return res
from odoo import api, fields, models
from odoo.exceptions import UserError


class StockPicking(models.Model):
    _inherit = "stock.picking"

    move_group_line = fields.One2many(
        "stock.move.group", "picking_id", "Stock")
    is_move_group_generated = fields.Boolean()

    @api.model
    def _create_new_move(self, move_group_line):
        move = self.move_ids_without_package[0].copy()
        move.product_id = move_group_line.product_id.id
        move.product_uom_qty = move_group_line.qty_total
        move.purchase_line_id = False
        return move
        
    @api.multi
    def _generate_move_group(self):
        for move_group_line in self.move_group_line:
            if not move_group_line.move_id.id:
                self._create_new_move(move_group_line)
            move = move_group_line.move_id
            qty_pack = move_group_line.qty_pack
            if qty_pack == 0:
                move_group_line.qty_pack = 1
                move_group_line.qty_done = move_group_line.qty_initial
            qty_done = 0
            for i in range(1, qty_pack + 1, 1):
                qty_done += getattr(move_group_line, "qty_detail" + str(i))
                move.product_uom_qty = getattr(
                    move_group_line, "qty_detail" + str(i))
                move.quantity_done = move.product_uom_qty
                move = move.copy()
            else:
                #                 if qty_done > move_group_line.qty_total:
                #                     move.product_uom_qty = qty_done - move_group_line.qty_total
                if qty_done < move_group_line.qty_total:
                    move.product_uom_qty = move_group_line.qty_total - qty_done
                else:
                    move.unlink()
            move = move_group_line.move_id
            if move.purchase_line_id:
                move.purchase_line_id.product_qty = move_group_line.qty_done
        self.is_move_group_generated = True

    @api.multi
    def button_validate(self):
        if not self.is_move_group_generated:
            self._generate_move_group()
        res = super(StockPicking, self).button_validate()
        return res
    
    @api.multi
    def action_done(self):
        res = super(StockPicking, self).action_done()
        invoice_line = []
        for group_id in self.move_group_line:
            move = group_id.move_id
            purchase_line_id = move.purchase_line_id 
            purchase_line_id.price_unit = group_id.price_unit
            purchase_line_id.product_qty = group_id.qty_done
            invoice_line.append([0,0, {'price_unit': group_id.price_unit,
                                       'product_id': group_id.product_id.id,
                                       'quantity': group_id.qty_done}])
        vals = {
                'journal_id': 1,
                'partner_id': self.partner_id.id,
                'invoice_line_ids': invoice_line
            }
        invoice = self.env['account.invoice'].with_context({'default_type': 'in_invoice', 'type': 'in_invoice', 'journal_type': 'purchase'}).create(vals)
        purchase_id = self.move_group_line.mapped('move').mapped("purchase_line_id").mapped("purchase_id")
        purchase_id.invoices_ids
        import ipdb;ipdb.set_trace()
        return res
    
class StockMoveGroup(models.Model):
    _name = "stock.move.group"
    _description = "Stock Move Group"
    
    picking_id = fields.Many2one("stock.picking", "Transfer")
    product_id = fields.Many2one("product.product", "Product")
    company_id = fields.Many2one(
        "res.company", "Company", default=lambda self: self.env.user.company_id.id)
    move_id = fields.Many2one("stock.move", "Stock Move")
    move_ids = fields.Many2many("stock.move", string="Stock Move")
    qty_pack = fields.Integer("Pack")
    qty_done = fields.Float("Diterima", compute="_compute_qty_done")
    qty_total = fields.Float("Jumlah Total")
    qty_initial = fields.Float() #deprecated
    price_unit = fields.Float("Harga Unit")
    price_total = fields.Float("Harga Total", compute="_compute_price_total")
    currency_id = fields.Many2one("res.currency", "Currency", default=lambda self: self.env.user.company_id.currency_id)
    state = fields.Selection([
        ('draft', 'New'), ('cancel', 'Cancelled'),
        ('waiting', 'Waiting Another Move'),
        ('confirmed', 'Waiting Availability'),
        ('partially_available', 'Partially Available'),
        ('assigned', 'Available'),
        ('done', 'Done')], string='Status',
        copy=False, default='draft', index=True, readonly=True,
        help="* New: When the stock move is created and not yet confirmed.\n"
             "* Waiting Another Move: This state can be seen when a move is waiting for another one, for example in a chained flow.\n"
             "* Waiting Availability: This state is reached when the procurement resolution is not straight forward. It may need the scheduler to run, a component to be manufactured...\n"
             "* Available: When products are reserved, it is set to \'Available\'.\n"
             "* Done: When the shipment is processed, the state is \'Done\'.")

    qty_detail1 = fields.Float("Detail1")
    qty_detail2 = fields.Float("Detail2")
    qty_detail3 = fields.Float("Detail3")
    qty_detail4 = fields.Float("Detail4")
    qty_detail5 = fields.Float("Detail5")
    qty_detail6 = fields.Float("Detail6")
    qty_detail7 = fields.Float("Detail7")
    qty_detail8 = fields.Float("Detail8")
    qty_detail9 = fields.Float("Detail9")
    qty_detail10 = fields.Float("Detail10")

    qty_detail11 = fields.Float("Detail11")
    qty_detail12 = fields.Float("Detail12")
    qty_detail13 = fields.Float("Detail13")
    qty_detail14 = fields.Float("Detail14")
    qty_detail15 = fields.Float("Detail15")
    qty_detail16 = fields.Float("Detail16")
    qty_detail17 = fields.Float("Detail17")
    qty_detail18 = fields.Float("Detail18")
    qty_detail19 = fields.Float("Detail19")
    qty_detail20 = fields.Float("Detail20")

    qty_detail21 = fields.Float("Detail21")
    qty_detail22 = fields.Float("Detail22")
    qty_detail23 = fields.Float("Detail23")
    qty_detail24 = fields.Float("Detail24")
    qty_detail25 = fields.Float("Detail25")
    qty_detail26 = fields.Float("Detail26")
    qty_detail27 = fields.Float("Detail27")
    qty_detail28 = fields.Float("Detail28")
    qty_detail29 = fields.Float("Detail29")
    qty_detail30 = fields.Float("Detail30")

    qty_detail31 = fields.Float("Detail31")
    qty_detail32 = fields.Float("Detail32")
    qty_detail33 = fields.Float("Detail33")
    qty_detail34 = fields.Float("Detail34")
    qty_detail35 = fields.Float("Detail35")
    qty_detail36 = fields.Float("Detail36")
    qty_detail37 = fields.Float("Detail37")
    qty_detail38 = fields.Float("Detail38")
    qty_detail39 = fields.Float("Detail39")
    qty_detail40 = fields.Float("Detail40")

    qty_detail41 = fields.Float("Detail41")
    qty_detail42 = fields.Float("Detail42")
    qty_detail43 = fields.Float("Detail43")
    qty_detail44 = fields.Float("Detail44")
    qty_detail45 = fields.Float("Detail45")
    qty_detail46 = fields.Float("Detail46")
    qty_detail47 = fields.Float("Detail47")
    qty_detail48 = fields.Float("Detail48")
    qty_detail49 = fields.Float("Detail49")
    qty_detail50 = fields.Float("Detail50")

    qty_detail51 = fields.Float("Detail51")
    qty_detail52 = fields.Float("Detail52")
    qty_detail53 = fields.Float("Detail53")
    qty_detail54 = fields.Float("Detail54")
    qty_detail55 = fields.Float("Detail55")
    qty_detail56 = fields.Float("Detail56")
    qty_detail57 = fields.Float("Detail57")
    qty_detail58 = fields.Float("Detail58")
    qty_detail59 = fields.Float("Detail59")
    qty_detail60 = fields.Float("Detail60")

    qty_detail61 = fields.Float("Detail61")
    qty_detail62 = fields.Float("Detail62")
    qty_detail63 = fields.Float("Detail63")
    qty_detail64 = fields.Float("Detail64")
    qty_detail65 = fields.Float("Detail65")
    qty_detail66 = fields.Float("Detail66")
    qty_detail67 = fields.Float("Detail67")
    qty_detail68 = fields.Float("Detail68")
    qty_detail69 = fields.Float("Detail69")
    qty_detail70 = fields.Float("Detail70")

    qty_detail71 = fields.Float("Detail71")
    qty_detail72 = fields.Float("Detail72")
    qty_detail73 = fields.Float("Detail73")
    qty_detail74 = fields.Float("Detail74")
    qty_detail75 = fields.Float("Detail75")
    qty_detail76 = fields.Float("Detail76")
    qty_detail77 = fields.Float("Detail77")
    qty_detail78 = fields.Float("Detail78")
    qty_detail79 = fields.Float("Detail79")
    qty_detail80 = fields.Float("Detail80")

    qty_detail81 = fields.Float("Detail81")
    qty_detail82 = fields.Float("Detail82")
    qty_detail83 = fields.Float("Detail83")
    qty_detail84 = fields.Float("Detail84")
    qty_detail85 = fields.Float("Detail85")
    qty_detail86 = fields.Float("Detail86")
    qty_detail87 = fields.Float("Detail87")
    qty_detail88 = fields.Float("Detail88")
    qty_detail89 = fields.Float("Detail89")
    qty_detail90 = fields.Float("Detail90")

    qty_detail91 = fields.Float("Detail91")
    qty_detail92 = fields.Float("Detail92")
    qty_detail93 = fields.Float("Detail93")
    qty_detail94 = fields.Float("Detail94")
    qty_detail95 = fields.Float("Detail95")
    qty_detail96 = fields.Float("Detail96")
    qty_detail97 = fields.Float("Detail97")
    qty_detail98 = fields.Float("Detail98")
    qty_detail99 = fields.Float("Detail99")
    qty_detail100 = fields.Float("Detail100")

    @api.onchange("qty_pack")
    def _onchange_pack(self):
        self._compute_qty_done()
        
    @api.depends("qty_detail1",
                 "qty_detail2",
                 "qty_detail3",
                 "qty_detail4",
                 "qty_detail5",
                 "qty_detail6",
                 "qty_detail7",
                 "qty_detail8",
                 "qty_detail9",
                 "qty_detail10",
                 "qty_detail11",
                 "qty_detail12",
                 "qty_detail13",
                 "qty_detail14",
                 "qty_detail15",
                 "qty_detail16",
                 "qty_detail17",
                 "qty_detail18",
                 "qty_detail19",
                 "qty_detail20",
                 "qty_detail21",
                 "qty_detail22",
                 "qty_detail23",
                 "qty_detail24",
                 "qty_detail25",
                 "qty_detail26",
                 "qty_detail27",
                 "qty_detail28",
                 "qty_detail29",
                 "qty_detail30",
                 "qty_detail31",
                 "qty_detail32",
                 "qty_detail33",
                 "qty_detail34",
                 "qty_detail35",
                 "qty_detail36",
                 "qty_detail37",
                 "qty_detail38",
                 "qty_detail39",
                 "qty_detail40",
                 "qty_detail41",
                 "qty_detail42",
                 "qty_detail43",
                 "qty_detail44",
                 "qty_detail45",
                 "qty_detail46",
                 "qty_detail47",
                 "qty_detail48",
                 "qty_detail49",
                 "qty_detail50",
                 "qty_detail51",
                 "qty_detail52",
                 "qty_detail53",
                 "qty_detail54",
                 "qty_detail55",
                 "qty_detail56",
                 "qty_detail57",
                 "qty_detail58",
                 "qty_detail59",
                 "qty_detail60",
                 "qty_detail61",
                 "qty_detail62",
                 "qty_detail63",
                 "qty_detail64",
                 "qty_detail65",
                 "qty_detail66",
                 "qty_detail67",
                 "qty_detail68",
                 "qty_detail69",
                 "qty_detail70",
                 "qty_detail71",
                 "qty_detail72",
                 "qty_detail73",
                 "qty_detail74",
                 "qty_detail75",
                 "qty_detail76",
                 "qty_detail77",
                 "qty_detail78",
                 "qty_detail79",
                 "qty_detail80",
                 "qty_detail81",
                 "qty_detail82",
                 "qty_detail83",
                 "qty_detail84",
                 "qty_detail85",
                 "qty_detail86",
                 "qty_detail87",
                 "qty_detail88",
                 "qty_detail89",
                 "qty_detail90",
                 "qty_detail91",
                 "qty_detail92",
                 "qty_detail93",
                 "qty_detail94",
                 "qty_detail95",
                 "qty_detail96",
                 "qty_detail97",
                 "qty_detail98",
                 "qty_detail99",
                 "qty_detail100")
    def _compute_qty_done(self):
        for rec in self:
            qty_done = 0
            for i in range(1, rec.qty_pack + 1, 1):
                qty_done += getattr(rec, "qty_detail" + str(i))
            rec.qty_done = qty_done

    @api.constrains("qty_detail1",
                    "qty_detail2",
                    "qty_detail3",
                    "qty_detail4",
                    "qty_detail5",
                    "qty_detail6",
                    "qty_detail7",
                    "qty_detail8",
                    "qty_detail9",
                    "qty_detail10",
                    "qty_detail11",
                    "qty_detail12",
                    "qty_detail13",
                    "qty_detail14",
                    "qty_detail15",
                    "qty_detail16",
                    "qty_detail17",
                    "qty_detail18",
                    "qty_detail19",
                    "qty_detail20",
                    "qty_detail21",
                    "qty_detail22",
                    "qty_detail23",
                    "qty_detail24",
                    "qty_detail25",
                    "qty_detail26",
                    "qty_detail27",
                    "qty_detail28",
                    "qty_detail29",
                    "qty_detail30",
                    "qty_detail31",
                    "qty_detail32",
                    "qty_detail33",
                    "qty_detail34",
                    "qty_detail35",
                    "qty_detail36",
                    "qty_detail37",
                    "qty_detail38",
                    "qty_detail39",
                    "qty_detail40",
                    "qty_detail41",
                    "qty_detail42",
                    "qty_detail43",
                    "qty_detail44",
                    "qty_detail45",
                    "qty_detail46",
                    "qty_detail47",
                    "qty_detail48",
                    "qty_detail49",
                    "qty_detail50",
                    "qty_detail51",
                    "qty_detail52",
                    "qty_detail53",
                    "qty_detail54",
                    "qty_detail55",
                    "qty_detail56",
                    "qty_detail57",
                    "qty_detail58",
                    "qty_detail59",
                    "qty_detail60",
                    "qty_detail61",
                    "qty_detail62",
                    "qty_detail63",
                    "qty_detail64",
                    "qty_detail65",
                    "qty_detail66",
                    "qty_detail67",
                    "qty_detail68",
                    "qty_detail69",
                    "qty_detail70",
                    "qty_detail71",
                    "qty_detail72",
                    "qty_detail73",
                    "qty_detail74",
                    "qty_detail75",
                    "qty_detail76",
                    "qty_detail77",
                    "qty_detail78",
                    "qty_detail79",
                    "qty_detail80",
                    "qty_detail81",
                    "qty_detail82",
                    "qty_detail83",
                    "qty_detail84",
                    "qty_detail85",
                    "qty_detail86",
                    "qty_detail87",
                    "qty_detail88",
                    "qty_detail89",
                    "qty_detail90",
                    "qty_detail91",
                    "qty_detail92",
                    "qty_detail93",
                    "qty_detail94",
                    "qty_detail95",
                    "qty_detail96",
                    "qty_detail97",
                    "qty_detail98",
                    "qty_detail99",
                    "qty_detail100")
    def _check_pack(self):
        for i in range(1, self.qty_pack + 1, 1):
            if getattr(self, "qty_detail" + str(i)) == 0.0:
                raise UserError("Jumlah barang %s tidak boleh 0 pada daftar kuantitas. Kurangi daftar kuantitas" % (
                    self.product_id.name))

    @api.depends("qty_done", "price_unit")
    def _compute_price_total(self):
        for rec in self:
            rec.price_total = rec.qty_done * rec.price_unit
        
class StockMoveLine(models.Model):
    _inherit = "stock.move.line"
    
    used_in_convection = fields.Boolean("Been used in convection")
    
from odoo import api, fields, models

class ResCompany(models.Model):
    _inherit = "res.company"
    
    cutting_limit_per_session = fields.Integer("Cutting Limit Per Session")
    
    
class ResConfigSettings(models.TransientModel):
    _inherit = "res.config.settings"
    
    cutting_limit_per_session = fields.Integer("Cutting Limit Per Session")
    
    
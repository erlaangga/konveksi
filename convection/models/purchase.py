from odoo import models, fields, api


class Purchase(models.Model):
    _inherit = "purchase.order"

    @api.multi
    def _create_picking(self):
        res = super(Purchase, self)._create_picking()
        for order in self:
            pickings = order.picking_ids.filtered(
                lambda x: x.state not in ('done', 'cancel'))
            if len(pickings) > 0:
                for picking in pickings:
                    for move in picking.move_lines:
                        group_vals = order._prepare_move_group_data(
                            picking, move)
                        order._create_move_group(group_vals)
        return res

    @api.multi
    def _prepare_move_group_data(self, picking, move):
        return {'product_id': move.product_id.id,
                'qty_total': move.product_qty,
                'qty_pack': 0,
                'picking_id': picking.id,
                'company_id': move.company_id.id,
                'move_id': move.id,
                'move_ids': [(6,0,move.ids)]
                }

    @api.multi
    def _create_move_group(self, vals):
        StockMoveGroup = self.env['stock.move.group']
        return StockMoveGroup.create(vals)

from odoo import models, fields, api


class GoodsMaterial(models.Model):
    _name = "goods.material"
    _description = "Material"
    _order = "name"
    
    name = fields.Char("NAMA", required=True)
    goods_stock = fields.Float("Stock")
    company_id = fields.Many2one("res.company", "Company", default=lambda self: self.env.user.company_id)
    currency_id = fields.Many2one("res.currency", "Currency", default=lambda self: self.env.user.company_id.currency_id)
    active = fields.Boolean("Active", default=True)
    notes = fields.Text("CATATAN")
    
    @api.multi
    def copy(self):
        raise UserError("Tidak boleh menggandakan data.")
    
    
class GoodsProduct(models.Model):
    _name = "goods.product"
    _description = "Goods"
    _order = "name"
    
    name = fields.Char("NAMA", required=True)
    customer_id = fields.Many2one("res.customer", "CUSTOMER")
    weight_process_cut = fields.Float("TINGKAT KESULITAN POTONG") #('Cut Process Weight')
    weight_process_sew = fields.Float("TINGKAT KESULITAN JAHIT") #('Sew Process Weight')
    goods_stock = fields.Float("Stok")
    company_id = fields.Many2one("res.company", "Company", default=lambda self: self.env.user.company_id)
    currency_id = fields.Many2one("res.currency", "Currency", default=lambda self: self.env.user.company_id.currency_id)
    active = fields.Boolean("Active", default=True)
    notes = fields.Text("CATATAN")
    
    @api.multi
    def name_get(self):
        result = []
        for rec in self:
            name = "(" + str(rec.customer_id.name) + ") " + str(rec.name)
            result.append((rec.id, name))
        return result
    
    @api.multi
    def copy(self):
        raise UserError("Tidak boleh menggandakan data.")
    
    
    
    
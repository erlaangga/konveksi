from odoo import models, fields, api


class Customer(models.Model):
    _name = "res.customer"
    _description = "Customer"
    _order = "name"
    
    name = fields.Char("Nama", required=True)
    address = fields.Char("Alamat")
    
    @api.multi
    def copy(self):
        raise UserError("Tidak boleh menggandakan data.")
    
    
class Supplier(models.Model):
    _name = "res.supplier"
    _description = "Supplier"
    _order = "name"
    
    name = fields.Char("Nama", required=True)
    address = fields.Char("Alamat")
    
    @api.multi
    def copy(self):
        raise UserError("Tidak boleh menggandakan data.")
    
    
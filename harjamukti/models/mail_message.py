from odoo.addons.mail.models.mail_message import Message
from odoo import api
from email.utils import formataddr

self = Message

@api.model
def _get_default_from(self):
    if self.env.user.email:
        return formataddr((self.env.user.name, self.env.user.email))
    else:
        return formataddr((self.env.user.name, 'no_email'))

self._get_default_from = _get_default_from
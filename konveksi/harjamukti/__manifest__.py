# -*- coding: utf-8 -*-
{
    'name': "Harjamukti",
    
    'summary': """
        Harjamukti""",
    
    'description': """
        Harjamukti
    """,
    
    'author': "Erlangga",
    'website': "https://erlaangga.github.io",
    
    # Categories can be used to filter modules in modules listing
    # Check https://github.com/odoo/odoo/blob/12.0/odoo/addons/base/data/ir_module_category_data.xml
    # for the full list
    'category': 'Administration',
    'version': '0.1',
    
    # any module necessary for this one to work correctly
    'depends': ['web', 'mail'], 
    
    # always loaded
    'data': [
        "data/ir_cron.xml",
        "data/ir_sequence.xml",
        "security/security.xml", 
        "security/ir.model.access.csv", 
        "views/partner_view.xml", 
        "views/employee_view.xml", 
        "views/goods_view.xml", 
        "views/transfer_view.xml", 
        "views/finish_material_view.xml",
        "views/finish_finish_view.xml", 
        "views/cut_material_view.xml", 
        "views/transfer_view.xml", 
        "views/menu.xml", 
        "views/custom.xml", 
        "report/transfer_goods_report.xml",
    ],
    'application': True,
}
from odoo import models, fields, api, tools
from datetime import datetime, timedelta


class Employee(models.Model):
    _inherit = "res.users"
    _description = "Employee"

    name = fields.Char("Nama", required=True)
    birth_date = fields.Date("Tanggal Lahir", default=fields.Date.today())
    birth_place = fields.Char("Tempat Lahir")
    age = fields.Integer("Umur")
    identity = fields.Char("NIK")
    production_team = fields.Selection(
        [("cutter", "Pemotong"),
         ("sewer", "Penjahit"),
         ("finishing", "Finishing"),
         ("manager", "Manajer")],
        "Bagian Produksi")
    inventory_team = fields.Selection(
        [("inventory_receipt", "Penerima Barang"),
         ("inventory_sent", "Finishing Barang"),
         ("manager", "Manajer")],
        "Bagian Gudang")
    # is_cutter = fields.Boolean("Pemotong")
    # is_sewer = fields.Boolean("Penjahit")
    # is_production_manager = fields.Boolean("Manajer Produksi")
    # is_inventory_receipt = fields.Boolean("Penerima")
    # is_inventory_sent = fields.Boolean("Finishing")
    # is_inventory_manager = fields.Boolean("Manajer Gudang")
    # is_admin = fields.Boolean("Admin")
    is_director = fields.Boolean("Direktur")
    salary_line = fields.One2many(
        "hr.employee.salary", "employee_id", "Daftar Gajian")
    payday = fields.Selection([("0", "Monday"), ("1", "Tuesday"), ("2", "Wednesday"), (
        "3", "Thursday"), ("4", "Friday"), ("5", "Saturday"), ("6", "Sunday")], "Hari Gajian", default="4")

    @api.model
    def _set_harjamukti_access(self, vals):
        ref = self.env.ref
        if vals.get('production_team') == "cutter":
            vals.update({"groups_id": [
                        (6, 0, [ref("harjamukti.group_cut_harjamukti").id, ref("base.group_user").id])]})
        elif vals.get('production_team') == "sewer":
            vals.update({"groups_id": [
                        (6, 0, [ref("harjamukti.group_sew_harjamukti").id, ref("base.group_user").id])]})
        elif vals.get('production_team') == "manager":
            vals.update({"groups_id": [
                        (6, 0, [ref("group_production_manager_harjamukti").id, ref("base.group_user").id])]})

        if vals.get('inventory_team') == "inventory_receipt":
            vals.update({"groups_id": [
                        (6, 0, [ref("harjamukti.group_cut_harjamukti").id, ref("base.group_user").id])]})
        elif vals.get('inventory_team') == "inventory_sent":
            vals.update({"groups_id": [
                        (6, 0, [ref("harjamukti.group_sew_harjamukti").id, ref("base.group_user").id])]})
        elif vals.get('inventory_team') == "manager":
            vals.update({"groups_id": [
                        (6, 0, [ref("group_inventory_manager_harjamukti").id, ref("base.group_user").id])]})

        if vals.get("is_director"):
            vals.update({"groups_id": [(6, 0, [
                        ref("harjamukti.group_director_harjamukti").id, ref("base.group_user").id])]})

    @api.model
    def create(self, vals):
        self._set_harjamukti_access(vals)
        res = super(Employee, self).create(vals)
        res.partner_id.lang = "id_ID"
        res.password = "1234"
        return res

    @api.multi
    def write(self, vals):
        self._set_harjamukti_access(vals)
        return super(Employee, self).write(vals)

    @api.model
    def auto_generate_payslip(self):
        return

    @api.multi
    def generate_payslip(self):
        the_day = datetime.now()
        year = the_day.year
        month = the_day.month
        day = the_day.day
        year_day = datetime(year, month, day) + timedelta(days=365)
        vals = {"employee_id": self.id,
                "date": the_day,
                "payday": self.payday
                }
        Salary = self.env['hr.employee.salary']
        while the_day < year_day:
            if Salary.search([("date", '=', str(the_day)), ("employee_id", "=", self.id)], limit=1):
                the_day = the_day + timedelta(days=7)
                continue
            elif str(the_day.weekday()) == self.payday:
                vals.update({'date': the_day})
                Salary.create(vals)
                the_day = the_day + timedelta(days=7)
            else:
                the_day = the_day + timedelta(days=1)

    @api.multi
    def copy(self):
        raise UserError("Tidak boleh menggandakan data.")

    
    def message_notify(self, partner_ids, body='', subject=False, **kwargs):
        """ Shortcut allowing to notify partners of messages not linked to
        any document. It pushes notifications on inbox or by email depending
        on the user configuration, like other notifications. """
        kw_author = kwargs.pop('author_id', False)
        if kw_author:
            author = self.env['res.partner'].sudo().browse(kw_author)
        else:
            author = self.env.user.partner_id
        # if not author.email:
        #     raise exceptions.UserError(_("Unable to notify message, please configure the sender's email address."))
        # email_from = formataddr((author.name, author.email))

        msg_values = {
            'subject': subject,
            'body': body,
            'author_id': author.id,
            # 'email_from': email_from,
            'message_type': 'notification',
            'partner_ids': partner_ids,
            'model': False,
            'subtype_id': self.env['ir.model.data'].xmlid_to_res_id('mail.mt_note'),
            'record_name': False,
            # 'reply_to': self.env['mail.thread']._notify_get_reply_to(default=email_from, records=None)[False],
            'message_id': tools.generate_tracking_message_id('message-notify'),
        }
        print('waw')
        msg_values.update(kwargs)
        return self.env['mail.thread'].message_post(**msg_values)

    def _message_log(self, body='', subject=False, message_type='notification', **kwargs):
        """ Shortcut allowing to post note on a document. It does not perform
        any notification and pre-computes some values to have a short code
        as optimized as possible. This method is private as it does not check
        access rights and perform the message creation as sudo to speedup
        the log process. This method should be called within methods where
        access rights are already granted to avoid privilege escalation. """
        if len(self.ids) > 1:
            raise exceptions.Warning(_('Invalid record set: should be called as model (without records) or on single-record recordset'))

        kw_author = kwargs.pop('author_id', False)
        if kw_author:
            author = self.env['res.partner'].sudo().browse(kw_author)
        else:
            author = self.env.user.partner_id
        # if not author.email:
        #     raise exceptions.UserError(_("Unable to log message, please configure the sender's email address."))
        # email_from = formataddr((author.name, author.email))

        message_values = {
            'subject': subject,
            'body': body,
            'author_id': author.id,
            # 'email_from': email_from,
            'message_type': message_type,
            'model': kwargs.get('model', self._name),
            'res_id': self.ids[0] if self.ids else False,
            'subtype_id': self.env['ir.model.data'].xmlid_to_res_id('mail.mt_note'),
            'record_name': False,
            # 'reply_to': self.env['mail.thread']._notify_get_reply_to(default=email_from, records=None)[False],
            'message_id': tools.generate_tracking_message_id('message-notify'),
        }
        print('waw')
        message_values.update(kwargs)
        message = self.env['mail.message'].sudo().create(message_values)
        return message
    
class EmployeeSalary(models.Model):
    _name = "hr.employee.salary"
    _description = "Gaji Karyawan"
    _rec_name = "employee_id"

    employee_id = fields.Many2one("res.users", "KARYAWAN", required=True)
    wage = fields.Float("GAJI")
    wage_bonus = fields.Float("BONUS")
    wage_net = fields.Float(
        "UANG DITERIMA", compute="_compute_wage_net", store=True)
    date = fields.Date("TANGGAL", default=fields.Date.today())
    is_lend = fields.Boolean("BERHUTANG")
    lent = fields.Boolean("TERHUTANG")
    loan_amount = fields.Float("JUMLAH BERHUTANG")
    loan_total = fields.Float(
        "JUMLAH TERHUTANG", compute="_compute_loan_total", store=True)
    is_pay = fields.Boolean("BAYAR HUTANG")
    loan_pay = fields.Float("JUMLAH BAYAR")
    payday = fields.Selection([("0", "Senin"), ("1", "Selasa"), ("2", "Rabu"), (
        "3", "Kamis"), ("4", "Jumat"), ("5", "Sabtu"), ("6", "Ahad")], "HARI GAJIAN", default="5")
    weekdate = fields.Integer("PEKAN KE-", compute="_compute_weekdate")
    is_wage_detail = fields.Boolean('GAJI BORONGAN')
    wage_1 = fields.Float("GAJI 1")
    wage_date_1 = fields.Date('TANGGAL 1')
    wage_2 = fields.Float("GAJI 2")
    wage_date_2 = fields.Date('TANGGAL 2')
    wage_3 = fields.Float("GAJI 3")
    wage_date_3 = fields.Date('TANGGAL 3')
    wage_4 = fields.Float("GAJI 4")
    wage_date_4 = fields.Date('TANGGAL 4')
    wage_5 = fields.Float("GAJI 5")
    wage_date_5 = fields.Date('TANGGAL 5')
    wage_6 = fields.Float("GAJI 6")
    wage_date_6 = fields.Date('TANGGAL 6')
    wage_7 = fields.Float("GAJI 7")
    wage_date_7 = fields.Date('TANGGAL 7')

    @api.depends("loan_amount", "loan_pay")
    def _compute_loan_total(self):
        for rec in self:
            if not rec.employee_id.id:
                return
            self._cr.execute(
                "select (sum(loan_amount) - sum(loan_pay)) from hr_employee_salary where employee_id=" + str(rec.employee_id.id))
            res = self._cr.fetchall()
            if res[0][0]:
                rec.loan_total = res[0][0]

    @api.depends("loan_amount", "loan_pay", "wage", "wage_1", "wage_2", "wage_3", "wage_4", "wage_5", "wage_6", "wage_7")
    def _compute_wage_net(self):
        for rec in self:
            rec.wage_net = rec.loan_amount + rec.wage + rec.wage_bonus - \
                rec.loan_pay + rec.wage_1 + rec.wage_2 + rec.wage_3 + \
                rec.wage_4 + rec.wage_5 + rec.wage_6 + rec.wage_7

    @api.multi
    def name_get(self):
        result = []
        for rec in self:
            name = rec.employee_id.name + ' ' + str(rec.date or '')
            result.append((rec.id, name))
        return result

# class EmployeeLoan(models.Model)

#     employee_id = fields.Many2one("hr.employee", "Karyawan")
#     amount = fields.Float("Jumlah")
#     date = fields.Date("Tanggal")

#
    @api.multi
    def copy(self):
        raise UserError("Tidak boleh menggandakan data.")

    @api.model
    def _generate_payslip(self):
        today = datetime.now()
        last_week = datetime.now() + timedelta(days=7)
        payday = str(today.weekday())
        employees = self.employee_id.search([('payday', '=', payday)])
        for employee in employees:
            wage = 0
            if employee.production_team == "cutter":
                wage = 1000
                quer = "select sum(product_qty_cut) from finish_material where date > '%s' and date < now()::date and user_id = %s;" %(str(last_week).split(' ')[0], str(employee.id))
                self._cr.execute(quer)
                wage = self._cr.fetchall()[0][0]
            elif employee.production_team == "sewer":
                wage = 2000
                quer = "select sum(product_qty_sew) from finish_material where date > '%s' and date < now()::date and sew_user_id = %s;" %(str(last_week).split(' ')[0], str(employee.id))
                self._cr.execute(quer)
                wage = self._cr.fetchall()[0][0]
            vals = {"employee_id": employee.id,
                    "date": str(today),
                    "wage": wage,
                    "payday": payday}
            self.create(vals)

    @api.depends("date")
    def _compute_weekdate(self):
        for rec in self:
            dt = rec.date
            rec.weekdate = dt.isocalendar()[1]


class ResPartner(models.Model):
    _inherit = "res.partner"
    
    
    def message_notify(self, partner_ids, body='', subject=False, **kwargs):
        """ Shortcut allowing to notify partners of messages not linked to
        any document. It pushes notifications on inbox or by email depending
        on the user configuration, like other notifications. """
        kw_author = kwargs.pop('author_id', False)
        if kw_author:
            author = self.env['res.partner'].sudo().browse(kw_author)
        else:
            author = self.env.user.partner_id
        # if not author.email:
        #     raise exceptions.UserError(_("Unable to notify message, please configure the sender's email address."))
        # email_from = formataddr((author.name, author.email))

        msg_values = {
            'subject': subject,
            'body': body,
            'author_id': author.id,
            # 'email_from': email_from,
            'message_type': 'notification',
            'partner_ids': partner_ids,
            'model': False,
            'subtype_id': self.env['ir.model.data'].xmlid_to_res_id('mail.mt_note'),
            'record_name': False,
            # 'reply_to': self.env['mail.thread']._notify_get_reply_to(default=email_from, records=None)[False],
            'message_id': tools.generate_tracking_message_id('message-notify'),
        }
        print('waw')
        msg_values.update(kwargs)
        return self.env['mail.thread'].message_post(**msg_values)

    def _message_log(self, body='', subject=False, message_type='notification', **kwargs):
        """ Shortcut allowing to post note on a document. It does not perform
        any notification and pre-computes some values to have a short code
        as optimized as possible. This method is private as it does not check
        access rights and perform the message creation as sudo to speedup
        the log process. This method should be called within methods where
        access rights are already granted to avoid privilege escalation. """
        if len(self.ids) > 1:
            raise exceptions.Warning(_('Invalid record set: should be called as model (without records) or on single-record recordset'))

        kw_author = kwargs.pop('author_id', False)
        if kw_author:
            author = self.env['res.partner'].sudo().browse(kw_author)
        else:
            author = self.env.user.partner_id
        # if not author.email:
        #     raise exceptions.UserError(_("Unable to log message, please configure the sender's email address."))
        # email_from = formataddr((author.name, author.email))

        message_values = {
            'subject': subject,
            'body': body,
            'author_id': author.id,
            # 'email_from': email_from,
            'message_type': message_type,
            'model': kwargs.get('model', self._name),
            'res_id': self.ids[0] if self.ids else False,
            'subtype_id': self.env['ir.model.data'].xmlid_to_res_id('mail.mt_note'),
            'record_name': False,
            # 'reply_to': self.env['mail.thread']._notify_get_reply_to(default=email_from, records=None)[False],
            'message_id': tools.generate_tracking_message_id('message-notify'),
        }
        print('waw')
        message_values.update(kwargs)
        message = self.env['mail.message'].sudo().create(message_values)
        return message
    
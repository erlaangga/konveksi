from odoo import models


class Follower(models.Model):
    _inherit = 'mail.followers'
    
    _sql_constraints = [
        ('mail_followers_res_partner_res_model_id_uniq', 'unique(res_model,res_id,partner_id,id)', 'Error, a partner cannot follow twice the same object.'),
        ] 

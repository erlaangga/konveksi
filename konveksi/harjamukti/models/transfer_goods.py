from odoo import models, fields, api, tools
from odoo.exceptions import UserError
from odoo.osv import expression

class TransferGoods(models.Model):
    _name = "transfer.goods"
    _description = "Goods Transfer"
    _inherit = ['mail.thread', 'mail.activity.mixin']
    _order = "id desc"
    
    name = fields.Char("No", required=False)
    goods_line = fields.One2many("transfer.goods.line", "transfer_id", " BARANG",  track_visibility='always')
    goods_group_line = fields.One2many("stock.move.group", "transfer_id", " ROLL PENERIMAAN", track_visibility='always')
    transfer_type = fields.Selection([("incoming", "Penerimaan"), ("outgoing", "Pengiriman")], "Transfer Type", track_visibility='always')
    state = fields.Selection([("draft", "Draft"), ("done", "Selesai"), ("cancel", "Cancel")], "Status", default="draft")
    date = fields.Date("TANGGAL", required=True, default=fields.Date.today(), track_visibility='always')
    state_payment = fields.Selection([('open', 'Belum Lunas'), ('paid', 'Terbayar')], 'Pembayaran', default='open')
    supplier_id = fields.Many2one("res.supplier", "SUPPLIER", track_visibility='always')
    customer_id = fields.Many2one("res.customer", "CUSTOMER", track_visibility='always')
    company_id = fields.Many2one("res.company", "COMPANY", default=lambda self: self.env.user.company_id)
    currency_id = fields.Many2one("res.currency", "CURRENCY", default=lambda self: self.env.user.company_id.currency_id)
    
    @api.multi
    def _generate_move_group(self):
        TGL = self.env['transfer.goods.line']
        for line in self.goods_group_line:
            qty_pack = line.qty_pack
            vals = {"goods_material_id": line.product_id.id, 
                    "goods_quantity": line.qty_done,
                    "qty_done": line.qty_done, 
                    "price_unit": line.price_unit,
                    "transfer_id": self.id,
                    "goods_color": line.goods_color,
                    "transfer_type": self.transfer_type,
                    "group_id": line.id,
                    }
            for i in range(1, qty_pack + 1, 1):
                vals.update({"qty_done": getattr(line, "qty_detail" + str(i))})
                TGL.create(vals)
    
    @api.multi
    def act_confirm(self):
        if self.transfer_type == "incoming":
            self._generate_move_group()
#         for group_line in self.goods_group_line:
#             group_line.state = 'done'
#         for line in self.goods_line:
#             line.state = "done"
#         self.state = "done"
            
    @api.model
    def create(self, vals):
        # vals.update({"name": self.env["ir.sequence"].next_by_code("transfer.receipt.goods")})
        res = super(TransferGoods, self).create(vals)
        res.act_confirm()
        return res
        
    @api.multi
    def name_get(self):
        result = []
        for rec in self:
            if rec.transfer_type == "incoming":
                name = str(rec.supplier_id.name) + ' (' + str(rec.date) + ')'
            else:
                name = str(rec.customer_id.name) + ' (' + str(rec.date) + ')'
            result.append((rec.id, name))
        return result
    
    @api.multi
    def copy(self):
        raise UserError("Tidak boleh menggandakan data.")
    
        
    def message_notify(self, partner_ids, body='', subject=False, **kwargs):
        """ Shortcut allowing to notify partners of messages not linked to
        any document. It pushes notifications on inbox or by email depending
        on the user configuration, like other notifications. """
        kw_author = kwargs.pop('author_id', False)
        if kw_author:
            author = self.env['res.partner'].sudo().browse(kw_author)
        else:
            author = self.env.user.partner_id
        # if not author.email:
        #     raise exceptions.UserError(_("Unable to notify message, please configure the sender's email address."))
        # email_from = formataddr((author.name, author.email))

        msg_values = {
            'subject': subject,
            'body': body,
            'author_id': author.id,
            # 'email_from': email_from,
            'message_type': 'notification',
            'partner_ids': partner_ids,
            'model': False,
            'subtype_id': self.env['ir.model.data'].xmlid_to_res_id('mail.mt_note'),
            'record_name': False,
            # 'reply_to': self.env['mail.thread']._notify_get_reply_to(default=email_from, records=None)[False],
            'message_id': tools.generate_tracking_message_id('message-notify'),
        }
        print('waw')
        msg_values.update(kwargs)
        return self.env['mail.thread'].message_post(**msg_values)

    def _message_log(self, body='', subject=False, message_type='notification', **kwargs):
        """ Shortcut allowing to post note on a document. It does not perform
        any notification and pre-computes some values to have a short code
        as optimized as possible. This method is private as it does not check
        access rights and perform the message creation as sudo to speedup
        the log process. This method should be called within methods where
        access rights are already granted to avoid privilege escalation. """
        if len(self.ids) > 1:
            raise exceptions.Warning(_('Invalid record set: should be called as model (without records) or on single-record recordset'))

        kw_author = kwargs.pop('author_id', False)
        if kw_author:
            author = self.env['res.partner'].sudo().browse(kw_author)
        else:
            author = self.env.user.partner_id
        # if not author.email:
        #     raise exceptions.UserError(_("Unable to log message, please configure the sender's email address."))
        # email_from = formataddr((author.name, author.email))
        message_values = {
            'subject': subject,
            'body': body,
            'author_id': author.id,
            # 'email_from': email_from,
            'message_type': message_type,
            'model': kwargs.get('model', self._name),
            'res_id': self.ids[0] if self.ids else False,
            'subtype_id': self.env['ir.model.data'].xmlid_to_res_id('mail.mt_note'),
            'record_name': False,
            # 'reply_to': self.env['mail.thread']._notify_get_reply_to(default=email_from, records=None)[False],
            'message_id': tools.generate_tracking_message_id('message-notify'),
        }
        print('waw')
        message_values.update(kwargs)
        message = self.env['mail.message'].sudo().create(message_values)
        return message
        

class TransferGoodsLine(models.Model):
    _name = "transfer.goods.line"
    _description = "Goods Transfer"
    _rec_name = "goods_material_id"
    
    goods_product_id = fields.Many2one("goods.product", "MODEL")
    goods_material_id = fields.Many2one("goods.material", "BARANG")
    goods_quantity = fields.Float("Jumlah Awal", digits=(16, 0))
    qty_done = fields.Float("JUMLAH", digits=(16, 0))
    price_unit = fields.Float("HARGA", digits=(16, 0))
    price_total = fields.Float("HARGA TOTAL", compute="_compute_price_total", digits=(16, 0))
    transfer_id = fields.Many2one("transfer.goods", "TRANSFER")
    group_id = fields.Many2one("stock.move.group", "Group")
    goods_color = fields.Char("WARNA")
    transfer_type = fields.Selection([("incoming", "Penerimaan"), ("outgoing", "Pengiriman")], "Transfer Type")
    date = fields.Date("TANGGAL", default=fields.Date.today())
    state = fields.Selection([("draft", "Draft"), ("done", "Done"), ("cancel", "Cancel")], "Status", default="draft")
    company_id = fields.Many2one("res.company", "Company", default=lambda self: self.env.user.company_id)
    currency_id = fields.Many2one("res.currency", "Currency", default=lambda self: self.env.user.company_id.currency_id)
    used_in_production = fields.Boolean('Terpakai')
    
    @api.multi
    def name_get(self):
        result = []
        for rec in self:
            if rec.transfer_type == "incoming":
                name = str(rec.goods_material_id.name)
            else:
                name = str(rec.goods_product_id.name)
            
            name += " [" + str(int(rec.qty_done)) + " Yard]"
            if rec.goods_color:
                name += " [" + rec.goods_color + "]"
#             if not self._context.get('production'):
#                 name +=  " (" + str(rec.date) + ")"
            result.append((rec.id, name))
        return result
        
    @api.depends("price_unit", "qty_done")
    def _compute_price_total(self):
        for rec in self:
            rec.price_total = rec.price_unit * rec.qty_done
    
    @api.model
    def create(self, vals):
        if vals.get('qty_done') and not vals.get('goods_quantity'):
            vals['goods_quantity'] = vals['qty_done']
        res = super(TransferGoodsLine, self).create(vals)
        return res
    
    @api.multi
    def write(self, vals):
        res = super(TransferGoodsLine, self).write(vals)
        if self.qty_done > self.goods_quantity:
            self.goods_quantity = self.qty_done
        super(TransferGoodsLine, self).write(vals)
    
    @api.multi
    def copy(self):
        raise UserError("Tidak boleh menggandakan data.")
    
    @api.model
    def _name_search(self, name='', args=None, operator='ilike', limit=100, name_get_uid=None):
        for nam in name.split(' '):
            if nam.isdigit():
                args += [('qty_done', '=', int(nam))]
            else:
                args += [('goods_material_id.name', 'ilike', nam)]
        connector = '|'
        if operator in expression.NEGATIVE_TERM_OPERATORS:
            connector = '&'   
        tgl = self._search(args, limit=limit, access_rights_uid=name_get_uid)
        return self.browse(tgl).name_get()
    
class StockMoveGroup(models.Model):
    _name = "stock.move.group"
    _description = "Stock Move Group"
    
#     picking_id = fields.Many2one("stock.picking", "Transfer")
    transfer_id = fields.Many2one("transfer.goods", "TRANSFER")
    product_id = fields.Many2one("goods.material", "BARANG", required=True)
    company_id = fields.Many2one(
        "res.company", "Company", default=lambda self: self.env.user.company_id.id)
#     move_id = fields.Many2one("stock.move", "Stock Move")
#     move_ids = fields.Many2many("stock.move", string="Stock Move")
    qty_pack = fields.Integer("ROLL")
    qty_done = fields.Float("TOTAL YARD", compute="_compute_qty_done", digits=(16, 0))
    qty_total = fields.Float("JUMLAH TOTAL", digits=(16, 0))
    price_unit = fields.Float("HARGA UNIT", digits=(16, 0))
    price_total = fields.Float("HARGA TOTAL", compute="_compute_price_total", digits=(16, 0))
    currency_id = fields.Many2one("res.currency", "Currency", default=lambda self: self.env.user.company_id.currency_id)
    goods_color = fields.Char("WARNA")
    transfer_goods_line_ids = fields.One2many("transfer.goods.line", "group_id", "TGL")
    
#     state = fields.Selection([
#         ('draft', 'New'), ('cancel', 'Cancelled'),
#         ('waiting', 'Waiting Another Move'),
#         ('confirmed', 'Waiting Availability'),
#         ('partially_available', 'Partially Available'),
#         ('assigned', 'Available'),
#         ('done', 'Done')], string='Status',
#         copy=False, default='draft', index=True, readonly=True,
#         help="* New: When the stock move is created and not yet confirmed.\n"
#              "* Waiting Another Move: This state can be seen when a move is waiting for another one, for example in a chained flow.\n"
#              "* Waiting Availability: This state is reached when the procurement resolution is not straight forward. It may need the scheduler to run, a component to be manufactured...\n"
#              "* Available: When products are reserved, it is set to \'Available\'.\n"
#              "* Done: When the shipment is processed, the state is \'Done\'.")

    qty_detail1 = fields.Float("Detail1", digits=(16, 0))
    qty_detail2 = fields.Float("Detail2", digits=(16, 0))
    qty_detail3 = fields.Float("Detail3", digits=(16, 0))
    qty_detail4 = fields.Float("Detail4", digits=(16, 0))
    qty_detail5 = fields.Float("Detail5", digits=(16, 0))
    qty_detail6 = fields.Float("Detail6", digits=(16, 0))
    qty_detail7 = fields.Float("Detail7", digits=(16, 0))
    qty_detail8 = fields.Float("Detail8", digits=(16, 0))
    qty_detail9 = fields.Float("Detail9", digits=(16, 0))
    qty_detail10 = fields.Float("Detail10", digits=(16, 0))

    qty_detail11 = fields.Float("Detail11", digits=(16, 0))
    qty_detail12 = fields.Float("Detail12", digits=(16, 0))
    qty_detail13 = fields.Float("Detail13", digits=(16, 0))
    qty_detail14 = fields.Float("Detail14", digits=(16, 0))
    qty_detail15 = fields.Float("Detail15", digits=(16, 0))
    qty_detail16 = fields.Float("Detail16", digits=(16, 0))
    qty_detail17 = fields.Float("Detail17", digits=(16, 0))
    qty_detail18 = fields.Float("Detail18", digits=(16, 0))
    qty_detail19 = fields.Float("Detail19", digits=(16, 0))
    qty_detail20 = fields.Float("Detail20", digits=(16, 0))

    qty_detail21 = fields.Float("Detail21", digits=(16, 0))
    qty_detail22 = fields.Float("Detail22", digits=(16, 0))
    qty_detail23 = fields.Float("Detail23", digits=(16, 0))
    qty_detail24 = fields.Float("Detail24", digits=(16, 0))
    qty_detail25 = fields.Float("Detail25", digits=(16, 0))
    qty_detail26 = fields.Float("Detail26", digits=(16, 0))
    qty_detail27 = fields.Float("Detail27", digits=(16, 0))
    qty_detail28 = fields.Float("Detail28", digits=(16, 0))
    qty_detail29 = fields.Float("Detail29", digits=(16, 0))
    qty_detail30 = fields.Float("Detail30", digits=(16, 0))

    qty_detail31 = fields.Float("Detail31", digits=(16, 0))
    qty_detail32 = fields.Float("Detail32", digits=(16, 0))
    qty_detail33 = fields.Float("Detail33", digits=(16, 0))
    qty_detail34 = fields.Float("Detail34", digits=(16, 0))
    qty_detail35 = fields.Float("Detail35", digits=(16, 0))
    qty_detail36 = fields.Float("Detail36", digits=(16, 0))
    qty_detail37 = fields.Float("Detail37", digits=(16, 0))
    qty_detail38 = fields.Float("Detail38", digits=(16, 0))
    qty_detail39 = fields.Float("Detail39", digits=(16, 0))
    qty_detail40 = fields.Float("Detail40", digits=(16, 0))

    qty_detail41 = fields.Float("Detail41", digits=(16, 0))
    qty_detail42 = fields.Float("Detail42", digits=(16, 0))
    qty_detail43 = fields.Float("Detail43", digits=(16, 0))
    qty_detail44 = fields.Float("Detail44", digits=(16, 0))
    qty_detail45 = fields.Float("Detail45", digits=(16, 0))
    qty_detail46 = fields.Float("Detail46", digits=(16, 0))
    qty_detail47 = fields.Float("Detail47", digits=(16, 0))
    qty_detail48 = fields.Float("Detail48", digits=(16, 0))
    qty_detail49 = fields.Float("Detail49", digits=(16, 0))
    qty_detail50 = fields.Float("Detail50", digits=(16, 0))

    qty_detail51 = fields.Float("Detail51", digits=(16, 0))
    qty_detail52 = fields.Float("Detail52", digits=(16, 0))
    qty_detail53 = fields.Float("Detail53", digits=(16, 0))
    qty_detail54 = fields.Float("Detail54", digits=(16, 0))
    qty_detail55 = fields.Float("Detail55", digits=(16, 0))
    qty_detail56 = fields.Float("Detail56", digits=(16, 0))
    qty_detail57 = fields.Float("Detail57", digits=(16, 0))
    qty_detail58 = fields.Float("Detail58", digits=(16, 0))
    qty_detail59 = fields.Float("Detail59", digits=(16, 0))
    qty_detail60 = fields.Float("Detail60", digits=(16, 0))

    qty_detail61 = fields.Float("Detail61", digits=(16, 0))
    qty_detail62 = fields.Float("Detail62", digits=(16, 0))
    qty_detail63 = fields.Float("Detail63", digits=(16, 0))
    qty_detail64 = fields.Float("Detail64", digits=(16, 0))
    qty_detail65 = fields.Float("Detail65", digits=(16, 0))
    qty_detail66 = fields.Float("Detail66", digits=(16, 0))
    qty_detail67 = fields.Float("Detail67", digits=(16, 0))
    qty_detail68 = fields.Float("Detail68", digits=(16, 0))
    qty_detail69 = fields.Float("Detail69", digits=(16, 0))
    qty_detail70 = fields.Float("Detail70", digits=(16, 0))

    qty_detail71 = fields.Float("Detail71", digits=(16, 0))
    qty_detail72 = fields.Float("Detail72", digits=(16, 0))
    qty_detail73 = fields.Float("Detail73", digits=(16, 0))
    qty_detail74 = fields.Float("Detail74", digits=(16, 0))
    qty_detail75 = fields.Float("Detail75", digits=(16, 0))
    qty_detail76 = fields.Float("Detail76", digits=(16, 0))
    qty_detail77 = fields.Float("Detail77", digits=(16, 0))
    qty_detail78 = fields.Float("Detail78", digits=(16, 0))
    qty_detail79 = fields.Float("Detail79", digits=(16, 0))
    qty_detail80 = fields.Float("Detail80", digits=(16, 0))

    qty_detail81 = fields.Float("Detail81", digits=(16, 0))
    qty_detail82 = fields.Float("Detail82", digits=(16, 0))
    qty_detail83 = fields.Float("Detail83", digits=(16, 0))
    qty_detail84 = fields.Float("Detail84", digits=(16, 0))
    qty_detail85 = fields.Float("Detail85", digits=(16, 0))
    qty_detail86 = fields.Float("Detail86", digits=(16, 0))
    qty_detail87 = fields.Float("Detail87", digits=(16, 0))
    qty_detail88 = fields.Float("Detail88", digits=(16, 0))
    qty_detail89 = fields.Float("Detail89", digits=(16, 0))
    qty_detail90 = fields.Float("Detail90", digits=(16, 0))

    qty_detail91 = fields.Float("Detail91", digits=(16, 0))
    qty_detail92 = fields.Float("Detail92", digits=(16, 0))
    qty_detail93 = fields.Float("Detail93", digits=(16, 0))
    qty_detail94 = fields.Float("Detail94", digits=(16, 0))
    qty_detail95 = fields.Float("Detail95", digits=(16, 0))
    qty_detail96 = fields.Float("Detail96", digits=(16, 0))
    qty_detail97 = fields.Float("Detail97", digits=(16, 0))
    qty_detail98 = fields.Float("Detail98", digits=(16, 0))
    qty_detail99 = fields.Float("Detail99", digits=(16, 0))
    qty_detail100 = fields.Float("Detail100", digits=(16, 0))

    @api.onchange("qty_pack")
    def _onchange_pack(self):
        self._compute_qty_done()
        
    @api.depends("qty_detail1",
                 "qty_detail2",
                 "qty_detail3",
                 "qty_detail4",
                 "qty_detail5",
                 "qty_detail6",
                 "qty_detail7",
                 "qty_detail8",
                 "qty_detail9",
                 "qty_detail10",
                 "qty_detail11",
                 "qty_detail12",
                 "qty_detail13",
                 "qty_detail14",
                 "qty_detail15",
                 "qty_detail16",
                 "qty_detail17",
                 "qty_detail18",
                 "qty_detail19",
                 "qty_detail20",
                 "qty_detail21",
                 "qty_detail22",
                 "qty_detail23",
                 "qty_detail24",
                 "qty_detail25",
                 "qty_detail26",
                 "qty_detail27",
                 "qty_detail28",
                 "qty_detail29",
                 "qty_detail30",
                 "qty_detail31",
                 "qty_detail32",
                 "qty_detail33",
                 "qty_detail34",
                 "qty_detail35",
                 "qty_detail36",
                 "qty_detail37",
                 "qty_detail38",
                 "qty_detail39",
                 "qty_detail40",
                 "qty_detail41",
                 "qty_detail42",
                 "qty_detail43",
                 "qty_detail44",
                 "qty_detail45",
                 "qty_detail46",
                 "qty_detail47",
                 "qty_detail48",
                 "qty_detail49",
                 "qty_detail50",
                 "qty_detail51",
                 "qty_detail52",
                 "qty_detail53",
                 "qty_detail54",
                 "qty_detail55",
                 "qty_detail56",
                 "qty_detail57",
                 "qty_detail58",
                 "qty_detail59",
                 "qty_detail60",
                 "qty_detail61",
                 "qty_detail62",
                 "qty_detail63",
                 "qty_detail64",
                 "qty_detail65",
                 "qty_detail66",
                 "qty_detail67",
                 "qty_detail68",
                 "qty_detail69",
                 "qty_detail70",
                 "qty_detail71",
                 "qty_detail72",
                 "qty_detail73",
                 "qty_detail74",
                 "qty_detail75",
                 "qty_detail76",
                 "qty_detail77",
                 "qty_detail78",
                 "qty_detail79",
                 "qty_detail80",
                 "qty_detail81",
                 "qty_detail82",
                 "qty_detail83",
                 "qty_detail84",
                 "qty_detail85",
                 "qty_detail86",
                 "qty_detail87",
                 "qty_detail88",
                 "qty_detail89",
                 "qty_detail90",
                 "qty_detail91",
                 "qty_detail92",
                 "qty_detail93",
                 "qty_detail94",
                 "qty_detail95",
                 "qty_detail96",
                 "qty_detail97",
                 "qty_detail98",
                 "qty_detail99",
                 "qty_detail100")
    def _compute_qty_done(self):
        for rec in self:
            qty_done = 0
            for i in range(1, rec.qty_pack + 1, 1):
                qty_done += getattr(rec, "qty_detail" + str(i))
            rec.qty_done = qty_done

    @api.constrains("qty_detail1",
                    "qty_detail2",
                    "qty_detail3",
                    "qty_detail4",
                    "qty_detail5",
                    "qty_detail6",
                    "qty_detail7",
                    "qty_detail8",
                    "qty_detail9",
                    "qty_detail10",
                    "qty_detail11",
                    "qty_detail12",
                    "qty_detail13",
                    "qty_detail14",
                    "qty_detail15",
                    "qty_detail16",
                    "qty_detail17",
                    "qty_detail18",
                    "qty_detail19",
                    "qty_detail20",
                    "qty_detail21",
                    "qty_detail22",
                    "qty_detail23",
                    "qty_detail24",
                    "qty_detail25",
                    "qty_detail26",
                    "qty_detail27",
                    "qty_detail28",
                    "qty_detail29",
                    "qty_detail30",
                    "qty_detail31",
                    "qty_detail32",
                    "qty_detail33",
                    "qty_detail34",
                    "qty_detail35",
                    "qty_detail36",
                    "qty_detail37",
                    "qty_detail38",
                    "qty_detail39",
                    "qty_detail40",
                    "qty_detail41",
                    "qty_detail42",
                    "qty_detail43",
                    "qty_detail44",
                    "qty_detail45",
                    "qty_detail46",
                    "qty_detail47",
                    "qty_detail48",
                    "qty_detail49",
                    "qty_detail50",
                    "qty_detail51",
                    "qty_detail52",
                    "qty_detail53",
                    "qty_detail54",
                    "qty_detail55",
                    "qty_detail56",
                    "qty_detail57",
                    "qty_detail58",
                    "qty_detail59",
                    "qty_detail60",
                    "qty_detail61",
                    "qty_detail62",
                    "qty_detail63",
                    "qty_detail64",
                    "qty_detail65",
                    "qty_detail66",
                    "qty_detail67",
                    "qty_detail68",
                    "qty_detail69",
                    "qty_detail70",
                    "qty_detail71",
                    "qty_detail72",
                    "qty_detail73",
                    "qty_detail74",
                    "qty_detail75",
                    "qty_detail76",
                    "qty_detail77",
                    "qty_detail78",
                    "qty_detail79",
                    "qty_detail80",
                    "qty_detail81",
                    "qty_detail82",
                    "qty_detail83",
                    "qty_detail84",
                    "qty_detail85",
                    "qty_detail86",
                    "qty_detail87",
                    "qty_detail88",
                    "qty_detail89",
                    "qty_detail90",
                    "qty_detail91",
                    "qty_detail92",
                    "qty_detail93",
                    "qty_detail94",
                    "qty_detail95",
                    "qty_detail96",
                    "qty_detail97",
                    "qty_detail98",
                    "qty_detail99",
                    "qty_detail100")
    def _check_pack(self):
        for rec in self:
            for i in range(1, rec.qty_pack + 1, 1):
                if getattr(rec, "qty_detail" + str(i)) == 0.0:
                    raise UserError("Detail roll %s tidak boleh 0, kurangi banyak roll diterima" % (
                        rec.product_id.name))

    @api.depends("qty_done", "price_unit")
    def _compute_price_total(self):
        for rec in self:
            rec.price_total = rec.qty_done * rec.price_unit
    
    @api.multi
    def copy(self):
        raise UserError("Tidak boleh menggandakan data.")
    
    @api.multi
    def unlink(self):
        for rec in self:
            rec.transfer_goods_line_ids
        return super(StockMoveGroup, self).unlink()
    
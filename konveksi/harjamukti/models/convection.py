# -*- coding: utf-8 -*-

from odoo import models, fields, api, tools
from odoo.exceptions import UserError


class CutMaterial(models.Model):
    _name = "cut.material"
    _description = "Material Cutting"
    _inherit = ['mail.thread', 'mail.activity.mixin']
    _order = "id desc"
    
#     @api.model
#     def _default_employee_id(self):
#         return self.env['hr.employee'].search([('user_id', '=', self.env.uid)], limit=1).id
    
    name = fields.Char("Name")
    user_id = fields.Many2one("res.users", "PEMOTONG", default=lambda self: self.env.uid, ondelete='restrict', track_visibility='always', domain="[('production_team', '=', 'cutter')]")#, readonly=True, store=True)
#     employee_id = fields.Many2one("hr.employee", "Pemotong", default=_default_employee_id, domain="[('user_id', '!=', user_id)]")
    state = fields.Selection([('draft', 'Draft'), ('confirmed', 'Confirmed'), (
        'cutting', 'Cutting'), ('check', 'Checking'), ('done', 'Done'), ('cancel', 'Batal')], 'State', default="draft")
    product_id = fields.Many2one('goods.product', 'MODEL', required=True, track_visibility='always')
    finish_material_ids = fields.One2many(
        'finish.material', 'cut_id', 'FINISH MATERIALS', copy=True, track_visibility='always')
    finish_material_group_ids = fields.One2many(
        'finish.material.group', 'cut_id', ' BAHAN MODEL', copy=True, track_visibility='always')
    finish_material_group_alls_ids = fields.One2many(
        'finish.material.group', 'cut_id', ' BAHAN MODEL ALL SIZE', copy=True)
    date = fields.Datetime("TANGGAL", default=fields.Datetime.now(), track_visibility='always')
#     product_material_ids= fields.One2many("goods.product", string="Material", compute="_compute_product_material")
    model_size = fields.Selection([('all_size', 'All Size'), ('non_all_size', 'Non All Size')], 'TIPE UKURAN', default="all_size", track_visibility='always')
    goods_color = fields.Char('WARNA')
    company_id = fields.Many2one("res.company", "Company", default=lambda self: self.env.user.company_id)
    
#     
#     @api.depends("employee_id")
#     def _get_user_employee(self):
#         for rec in self:
#             rec.user_id = rec.employee_id.user_id
#     
#     @api.depends("product_id")
#     def _compute_product_material(self):
#         for rec in self:
#             material_product = self.env['product.product'].search([('product_tmpl_id', 'in', rec.product_id.clothing_material_line.ids)])
#             rec.product_material_ids = material_product.ids
#         
    @api.multi
    def act_confirm(self):
        pass
#         now = fields.Datetime.now()
#         for line in self.finish_material_ids:
#             if not line.date:
#                 line.date = now
#                 line.act_check()
#             move_data = self._prepare_move_data(line)
#             move = self.env['stock.move'].create(move_data)
#             line.move_id = move.id
#             move._action_assign()
        # self.state = 'confirmed'

    @api.multi
    def act_cut(self):
        self.state = 'cutting'

    @api.multi
    def act_check(self):
        self.state = 'check'

    @api.multi
    def act_done(self):
        self.state = 'done'

    @api.multi
    def act_cancel(self):
        self.state = 'cancel'

    @api.multi
    def act_draft(self):
        self.state = 'draft'
# 
#     @api.model
#     def create(self, vals):
#         # name = self.env["ir.sequence"].next_by_code("cut.material")
#         # vals.update({'name': name})
#         return super(CutMaterial, self).create(vals)
#     
#     @api.multi
#     def _prepare_move_data(self, line):
#         move_line = line.move_line_id
#         move_id = line.move_line_id.move_id
#         picking_type_id = self.env['stock.picking.type'].search([('code', '=', 'internal')], limit=1)
#         location_id = self.env['stock.location'].search([('usage', '=', 'production')], limit=1)
#         template = {
#             'name': self.name or '',
#             'product_id': move_line.product_id.id,
#             'product_uom': move_line.product_id.uom_id.id,
#             'product_uom_qty': move_line.product_uom_qty,
#             'date': line.date,
#             'date_expected': line.date,
#             'location_id': move_line.location_dest_id.id,
#             'location_dest_id': location_id.id,
#             'partner_id': line.user_id.partner_id.id,
#             'move_orig_ids': [(6, 0, move_id.ids)],
#             'state': 'draft',
#             'purchase_line_id': move_id.purchase_line_id.id,
#             'company_id': move_id.company_id.id,
#             'price_unit': move_id.purchase_line_id.price_unit,
#             'picking_type_id': picking_type_id.id,
#             # 'group_id': self.order_id.group_id.id,
#             'origin': self.name,
#             # 'route_ids': self.order_id.picking_type_id.warehouse_id and [(6, 0, [x.id for x in self.order_id.picking_type_id.warehouse_id.route_ids])] or [],
#             'warehouse_id': move_id.picking_type_id.warehouse_id.id,
#             # 'procure_method': '',
#         }
#         return template
#     
    @api.multi
    def name_get(self):
        result = []
        for rec in self:
            name = " (" + str(rec.user_id.name) + ") " + str(rec.product_id.name) + " " + str(rec.date)
            result.append((rec.id, name))
        return result
    
    @api.multi
    def copy(self):
        raise UserError("Tidak boleh menggandakan data.")
    
    def message_notify(self, partner_ids, body='', subject=False, **kwargs):
        """ Shortcut allowing to notify partners of messages not linked to
        any document. It pushes notifications on inbox or by email depending
        on the user configuration, like other notifications. """
        kw_author = kwargs.pop('author_id', False)
        if kw_author:
            author = self.env['res.partner'].sudo().browse(kw_author)
        else:
            author = self.env.user.partner_id
        # if not author.email:
        #     raise exceptions.UserError(_("Unable to notify message, please configure the sender's email address."))
        # email_from = formataddr((author.name, author.email))

        msg_values = {
            'subject': subject,
            'body': body,
            'author_id': author.id,
            # 'email_from': email_from,
            'message_type': 'notification',
            'partner_ids': partner_ids,
            'model': False,
            'subtype_id': self.env['ir.model.data'].xmlid_to_res_id('mail.mt_note'),
            'record_name': False,
            # 'reply_to': self.env['mail.thread']._notify_get_reply_to(default=email_from, records=None)[False],
            'message_id': tools.generate_tracking_message_id('message-notify'),
        }
        print('waw')
        msg_values.update(kwargs)
        return self.env['mail.thread'].message_post(**msg_values)

    def _message_log(self, body='', subject=False, message_type='notification', **kwargs):
        """ Shortcut allowing to post note on a document. It does not perform
        any notification and pre-computes some values to have a short code
        as optimized as possible. This method is private as it does not check
        access rights and perform the message creation as sudo to speedup
        the log process. This method should be called within methods where
        access rights are already granted to avoid privilege escalation. """
        if len(self.ids) > 1:
            raise exceptions.Warning(_('Invalid record set: should be called as model (without records) or on single-record recordset'))

        kw_author = kwargs.pop('author_id', False)
        if kw_author:
            author = self.env['res.partner'].sudo().browse(kw_author)
        else:
            author = self.env.user.partner_id
        # if not author.email:
        #     raise exceptions.UserError(_("Unable to log message, please configure the sender's email address."))
        # email_from = formataddr((author.name, author.email))

        message_values = {
            'subject': subject,
            'body': body,
            'author_id': author.id,
            # 'email_from': email_from,
            'message_type': message_type,
            'model': kwargs.get('model', self._name),
            'res_id': self.ids[0] if self.ids else False,
            'subtype_id': self.env['ir.model.data'].xmlid_to_res_id('mail.mt_note'),
            'record_name': False,
            # 'reply_to': self.env['mail.thread']._notify_get_reply_to(default=email_from, records=None)[False],
            'message_id': tools.generate_tracking_message_id('message-notify'),
        }
        print('waw')
        message_values.update(kwargs)
        message = self.env['mail.message'].sudo().create(message_values)
        return message


class FinishMaterial(models.Model):
    _name = "finish.material"
    _description = "Material Finishing"
    _inherit = ['mail.thread', 'mail.activity.mixin']
    _order = "id desc"
    
    name = fields.Char("Name")
    image = fields.Binary("FOTO", track_visibility='always', related="group_id.image")
    user_id = fields.Many2one("res.users", "PEMOTONG", default=lambda self: self.env.uid, ondelete='restrict', track_visibility='always', domain="[('production_team', '=', 'cutter')]")
    sew_user_id = fields.Many2one("res.users", "PENJAHIT", ondelete='restrict', track_visibility='always', domain="[('production_team', '=', 'sewer')]")
    finish_user_id = fields.Many2one("res.users", "CHECKER")
    cut_id = fields.Many2one('cut.material', 'CUTTING', ondelete='cascade')
    group_id = fields.Many2one('finish.material.group', 'GROUP', ondelete='cascade', track_visibility='always')
    # state = fields.Selection([('draft', 'Draft'), ('sew', 'Sewing'),
    #                           ('check', 'check'), ('finish', 'Finish')], 'State', default="draft")
    state = fields.Selection([('draft', 'Draft'), ('assign', 'Menunggu Penjahit'), ('sew', 'Jahit'), ('check', 'Check'), ('done', 'Selesai')], 'STATUS JAHIT', default="draft")
    date = fields.Datetime("TANGGAL POTONG", default=fields.Datetime.now())
    date_sew = fields.Datetime("TANGGAL JAHIT", track_visibility='always')
    date_finish = fields.Datetime("Finish Date")
#     product_id = fields.Many2one('goods.product', 'Bahan', compute="_get_product", store=True, readonly=True)
    move_line_id = fields.Many2one('transfer.goods.line', 'BARANG', domain="[('transfer_type', '=', 'incoming'), ('state', '=', 'done'), ('used_in_production', '=', False)]", track_visibility='always')
    goods_product_id = fields.Many2one("goods.product", "MODEL", related="cut_id.product_id", store=True, readonly=True, track_visibility='always')
    goods_color = fields.Char("WARNA", track_visibility='always')
    size = fields.Selection([("xs", "XS"), ("s", "S"), ("m", "M"), ("l", "L"), ("xl", "XL"), ("xxl", "XXL"), ("other", "Other"), ("all_size", "All Size")], "UKURAN", track_visibility='always')
    product_qty_cut = fields.Float("JUMLAH POTONGAN", track_visibility='always')
    product_qty_sew = fields.Float("JUMLAH JAHITAN", track_visibility='always')
    product_qty = fields.Float("JUMLAH", compute="_get_move_qty", store=True)
    company_id = fields.Many2one("res.company", "Company", default=lambda self: self.env.user.company_id)
    notes = fields.Char("CATATAN", track_visibility='always')
    notes_sew = fields.Char("CATATAN ", track_visibility='always')
    
    @api.depends("move_line_id")
    def _get_move_qty(self):
        for rec in self:
            rec.product_qty = rec.move_line_id.qty_done

#     @api.depends("move_line_id")
#     def _get_product(self):
#         for rec in self:
#             rec.product_id = rec.move_line_id.product_id.id
#     
    @api.multi
    def act_check(self):
        self.state = "check"

    @api.multi
    def act_sew(self):
        if not self.sew_user_id.id:
            self.sew_user_id = self.env.uid
        self.state = 'sew'
    
    @api.multi
    def act_finish(self):
        self.state = 'finish'
    
#     @api.model
#     def create(self, vals):
#         # name = self.env["ir.sequence"].next_by_code("finish.material")
#         # vals.update({'name': name})
#         return super(FinishMaterial, self).create(vals)
    
    @api.multi
    def name_get(self):
        result = []
        for rec in self:
            name = str(rec.cut_id.product_id.name) + ' (' + str(rec.date) + ')'
            result.append((rec.id, name))
        return result
    
    @api.multi
    def copy(self):
        raise UserError("Tidak boleh menggandakan data.")

    def message_notify(self, partner_ids, body='', subject=False, **kwargs):
        """ Shortcut allowing to notify partners of messages not linked to
        any document. It pushes notifications on inbox or by email depending
        on the user configuration, like other notifications. """
        kw_author = kwargs.pop('author_id', False)
        if kw_author:
            author = self.env['res.partner'].sudo().browse(kw_author)
        else:
            author = self.env.user.partner_id
        # if not author.email:
        #     raise exceptions.UserError(_("Unable to notify message, please configure the sender's email address."))
        # email_from = formataddr((author.name, author.email))

        msg_values = {
            'subject': subject,
            'body': body,
            'author_id': author.id,
            # 'email_from': email_from,
            'message_type': 'notification',
            'partner_ids': partner_ids,
            'model': False,
            'subtype_id': self.env['ir.model.data'].xmlid_to_res_id('mail.mt_note'),
            'record_name': False,
            # 'reply_to': self.env['mail.thread']._notify_get_reply_to(default=email_from, records=None)[False],
            'message_id': tools.generate_tracking_message_id('message-notify'),
        }
        print('waw')
        msg_values.update(kwargs)
        return self.env['mail.thread'].message_post(**msg_values)

    def _message_log(self, body='', subject=False, message_type='notification', **kwargs):
        """ Shortcut allowing to post note on a document. It does not perform
        any notification and pre-computes some values to have a short code
        as optimized as possible. This method is private as it does not check
        access rights and perform the message creation as sudo to speedup
        the log process. This method should be called within methods where
        access rights are already granted to avoid privilege escalation. """
        if len(self.ids) > 1:
            raise exceptions.Warning(_('Invalid record set: should be called as model (without records) or on single-record recordset'))

        kw_author = kwargs.pop('author_id', False)
        if kw_author:
            author = self.env['res.partner'].sudo().browse(kw_author)
        else:
            author = self.env.user.partner_id
        # if not author.email:
        #     raise exceptions.UserError(_("Unable to log message, please configure the sender's email address."))
        # email_from = formataddr((author.name, author.email))

        message_values = {
            'subject': subject,
            'body': body,
            'author_id': author.id,
            # 'email_from': email_from,
            'message_type': message_type,
            'model': kwargs.get('model', self._name),
            'res_id': self.ids[0] if self.ids else False,
            'subtype_id': self.env['ir.model.data'].xmlid_to_res_id('mail.mt_note'),
            'record_name': False,
            # 'reply_to': self.env['mail.thread']._notify_get_reply_to(default=email_from, records=None)[False],
            'message_id': tools.generate_tracking_message_id('message-notify'),
        }
        print('waw')
        message_values.update(kwargs)
        message = self.env['mail.message'].sudo().create(message_values)
        return message
        
class FinishMaterialLine(models.Model):
    _name = "finish.material.line"
    _description = "Material Finishing Detail"
    
    finish_id = fields.Many2one("finish.material", "FINISH")
    product_id = fields.Many2one("goods.product", "MODEL")
    product_qty = fields.Float("JUMLAH")
    employee_id = fields.Many2one("res.users", "KARYAWAN")
    company_id = fields.Many2one("res.company", "Company", default=lambda self: self.env.user.company_id)
    notes = fields.Char("CATATAN")
    
    @api.multi
    def act_take(self):
        for rec in self:
            rec.user_id = self.env.uid
# class convection(models.Model):
#     _name = 'convection.convection'

#     name = fields.Char()
#     value = fields.Integer()
#     value2 = fields.Float(compute="_value_pc", store=True)
#     description = fields.Text()
#
#     @api.depends('value')
#     def _value_pc(self):
#         self.value2 = float(self.value) / 100

class FinishMaterialGroup(models.Model):
    _name = "finish.material.group"
    _description = "Material Finishing"
    
    name = fields.Char("NAME")
    image = fields.Binary("FOTO")
    image_big = fields.Binary("FOTO ASLI")
    user_id = fields.Many2one("res.users", "PEMOTONG", default=lambda self: self.env.uid)
    date = fields.Datetime("TANGGAL", default=fields.Datetime.now())
    cut_id = fields.Many2one('cut.material', 'Cutting')
#     product_id = fields.Many2one('goods.product', 'Bahan', compute="_get_product", store=True, readonly=True)
    move_line_id = fields.Many2one('transfer.goods.line', 'BARANG', domain="[('transfer_type', '=', 'incoming'), ('state', '=', 'done'), ('used_in_production', '=', False)]")
    finish_material_ids = fields.One2many("finish.material", "group_id", "Daftar Jahitan")
    product_qty_cut_xs = fields.Float("XS", digits=(16, 0))
    product_qty_cut_s = fields.Float("S", digits=(16, 0))
    product_qty_cut_m = fields.Float("M", digits=(16, 0))
    product_qty_cut_l = fields.Float("L", digits=(16, 0))
    product_qty_cut_xl = fields.Float("XL", digits=(16, 0))
    product_qty_cut_xxl = fields.Float("XXL", digits=(16, 0))
    product_qty_cut_other = fields.Float("LAIN-LAIN", digits=(16, 0))
    product_qty_cut_total = fields.Float("TOTAL POTONG", compute="_compute_total_cut", digits=(16, 0))
    product_qty = fields.Float("YARD", compute="_get_move_qty", digits=(16, 0), store=True)
    company_id = fields.Many2one("res.company", "Company", default=lambda self: self.env.user.company_id)
    notes = fields.Char("CATATAN", track_visibility='always')

    @api.depends("product_qty_cut_xs", "product_qty_cut_s", "product_qty_cut_m", "product_qty_cut_l", "product_qty_cut_xl", "product_qty_cut_xxl", "product_qty_cut_other")
    def _compute_total_cut(self):
        for rec in self:
            product_qty_cut_total = 0
            product_qty_cut_total += rec.product_qty_cut_xs
            product_qty_cut_total += rec.product_qty_cut_s
            product_qty_cut_total += rec.product_qty_cut_m
            product_qty_cut_total += rec.product_qty_cut_l
            product_qty_cut_total += rec.product_qty_cut_xl
            product_qty_cut_total += rec.product_qty_cut_xxl
            product_qty_cut_total += rec.product_qty_cut_other
            rec.product_qty_cut_total = product_qty_cut_total
            
            
    @api.depends("move_line_id")
    def _get_move_qty(self):
        for rec in self:
            rec.product_qty = rec.move_line_id.qty_done
#     
#     @api.depends("move_line_id")
#     def _get_product(self):
#         for rec in self:
#             rec.product_id = rec.move_line_id.product_id.id
#     
    @api.multi
    def act_check(self):
        self.state = "check"

    @api.multi
    def act_sew(self):
        self.state = 'sew'
    
    @api.multi
    def act_finish(self):
        self.state = 'finish'
    
#     @api.model
#     def create(self, vals):
#         # name = self.env["ir.sequence"].next_by_code("finish.material")
#         # vals.update({'name': name})
#         return super(FinishMaterial, self).create(vals)
    
    
    @api.multi
    def copy(self):
        raise UserError("Tidak boleh menggandakan data.")
    
    @api.model
    def create(self, vals):
        vals['image_big'] = vals.get("image")
        if vals.get('image'):
            tools.image_resize_images(vals, sizes={'image': (40, None)})
        res = super(FinishMaterialGroup, self).create(vals)
        FinishMaterial = self.env['finish.material']
        other_size = res.cut_id.model_size == "all_size" and "all_size" or "other"
        res.move_line_id.used_in_production = True
        vals = {"move_line_id": res.move_line_id.id,
                "group_id": res.id,
                "user_id": res.user_id.id,
                "state": 'assign',
                "cut_id": res.cut_id.id,
                }
        if res.cut_id.model_size != 'all_size':
            if res.product_qty_cut_xs:
                vals.update({"product_qty_cut": res.product_qty_cut_xs, "size": "xs"})
                # FinishMaterial |= 
                FinishMaterial.create(vals)
            if res.product_qty_cut_s:
                vals.update({"product_qty_cut": res.product_qty_cut_s, "size": "s"})
                # FinishMaterial |= 
                FinishMaterial.create(vals)
            if res.product_qty_cut_m:
                vals.update({"product_qty_cut": res.product_qty_cut_m, "size": "m"})
                # FinishMaterial |= 
                FinishMaterial.create(vals)
            if res.product_qty_cut_l:
                vals.update({"product_qty_cut": res.product_qty_cut_l, "size": "l"})
                # FinishMaterial |= 
                FinishMaterial.create(vals)
                vals.update({"product_qty_cut": res.product_qty_cut_xl, "size": "xl"})
            if res.product_qty_cut_xl:
                # FinishMaterial |= 
                FinishMaterial.create(vals)
            if res.product_qty_cut_xxl:
                vals.update({"product_qty_cut": res.product_qty_cut_xxl, "size": "xxl"})
                # FinishMaterial |= 
                FinishMaterial.create(vals)
        if res.product_qty_cut_other:
            vals.update({"product_qty_cut": res.product_qty_cut_other, "size": other_size})
            # FinishMaterial |= 
            FinishMaterial.create(vals)
        if res.notes:
            res.cut_id.product_id.notes = res.notes
        return res
    
    @api.multi
    def write(self, vals):
        vals['image_big'] = vals.get("image")
        if vals.get('image'):
            tools.image_resize_images(vals, sizes={'image': (40, None)})
        if vals.get('product_qty_cut_xs') or vals.get('product_qty_cut_xs') == 0:
            qty_cut = vals['product_qty_cut_xs']
            line = self.finish_material_ids.filtered(lambda line: line.product_qty_cut == self.product_qty_cut_xs and line.size == "xs")
            if vals.get('product_qty_cut_xs') == 0:
                line.unlink()
            elif len(line.ids) == 1:
                line.product_qty_cut = qty_cut
            else:
                fm_val = {"move_line_id": self.move_line_id.id,
                        "group_id": self.id,
                        "user_id": self.user_id.id,
                        "state": 'assign',
                        "cut_id": self.cut_id.id,
                        "product_qty_cut": qty_cut,
                        "size": "xs"
                        }
                self.env['finish.material'].create(fm_val)
        if vals.get('product_qty_cut_s') or vals.get('product_qty_cut_s') == 0:
            qty_cut = vals['product_qty_cut_s']
            line = self.finish_material_ids.filtered(lambda line: line.product_qty_cut == self.product_qty_cut_s and line.size == "s")
            if vals.get('product_qty_cut_s') == 0:
                line.unlink()
            elif len(line.ids) == 1:
                line.product_qty_cut = qty_cut
            else:
                fm_val = {"move_line_id": self.move_line_id.id,
                        "group_id": self.id,
                        "user_id": self.user_id.id,
                        "state": 'assign',
                        "cut_id": self.cut_id.id,
                        "product_qty_cut": qty_cut,
                        "size": "s"
                        }
                self.env['finish.material'].create(fm_val)
        if vals.get('product_qty_cut_m') or vals.get('product_qty_cut_m') == 0:
            qty_cut = vals['product_qty_cut_m']
            line = self.finish_material_ids.filtered(lambda line: line.product_qty_cut == self.product_qty_cut_m and line.size == "m")
            if vals.get('product_qty_cut_m') == 0:
                line.unlink()
            elif len(line.ids) == 1:
                line.product_qty_cut = qty_cut
            else:
                fm_val = {"move_line_id": self.move_line_id.id,
                        "group_id": self.id,
                        "user_id": self.user_id.id,
                        "state": 'assign',
                        "cut_id": self.cut_id.id,
                        "product_qty_cut": qty_cut,
                        "size": "m"
                        }
                self.env['finish.material'].create(fm_val)
        if vals.get('product_qty_cut_l') or vals.get('product_qty_cut_l') == 0:
            qty_cut = vals['product_qty_cut_l']
            line = self.finish_material_ids.filtered(lambda line: line.product_qty_cut == self.product_qty_cut_l and line.size == "l")
            if vals.get('product_qty_cut_l') == 0:
                line.unlink()
            elif len(line.ids) == 1:
                line.product_qty_cut = qty_cut
            else:
                fm_val = {"move_line_id": self.move_line_id.id,
                        "group_id": self.id,
                        "user_id": self.user_id.id,
                        "state": 'assign',
                        "cut_id": self.cut_id.id,
                        "product_qty_cut": qty_cut,
                        "size": "l"
                        }
                self.env['finish.material'].create(fm_val)
        if vals.get('product_qty_cut_xl') or vals.get('product_qty_cut_xl') == 0:
            qty_cut = vals['product_qty_cut_xl']
            line = self.finish_material_ids.filtered(lambda line: line.product_qty_cut == self.product_qty_cut_xl and line.size == "xl")
            if vals.get('product_qty_cut_xl') == 0:
                line.unlink()
            elif len(line.ids) == 1:
                line.product_qty_cut = qty_cut
            else:
                fm_val = {"move_line_id": self.move_line_id.id,
                        "group_id": self.id,
                        "user_id": self.user_id.id,
                        "state": 'assign',
                        "cut_id": self.cut_id.id,
                        "product_qty_cut": qty_cut,
                        "size": "xl"
                        }
                self.env['finish.material'].create(fm_val)
        if vals.get('product_qty_cut_xxl') or vals.get('product_qty_cut_xxl') == 0:
            qty_cut = vals['product_qty_cut_xxl']
            line = self.finish_material_ids.filtered(lambda line: line.product_qty_cut == self.product_qty_cut_xxl and line.size == "xxl")
            if vals.get('product_qty_cut_xxl') == 0:
                line.unlink()
            elif len(line.ids) == 1:
                line.product_qty_cut = qty_cut
            else:
                fm_val = {"move_line_id": self.move_line_id.id,
                        "group_id": self.id,
                        "user_id": self.user_id.id,
                        "state": 'assign',
                        "cut_id": self.cut_id.id,
                        "product_qty_cut": qty_cut,
                        "size": "xxl"
                        }
                self.env['finish.material'].create(fm_val)
        if vals.get('product_qty_cut_other') or vals.get('product_qty_cut_other') == 0:
            other_size = self.cut_id.model_size == "all_size" and "all_size" or "other"
            qty_cut = vals["product_qty_cut_other"]
            line = self.finish_material_ids.filtered(lambda line: line.product_qty_cut == self.product_qty_cut_other and line.size in ('other', 'all_size'))
            if vals.get('product_qty_cut_other') == 0:
                line.unlink()
            elif line.id:
                line.product_qty_cut = qty_cut
            else:
                fm_val = {"move_line_id": self.move_line_id.id,
                        "group_id": self.id,
                        "user_id": self.user_id.id,
                        "state": 'assign',
                        "cut_id": self.cut_id.id,
                        "product_qty_cut": qty_cut,
                        "size": other_size
                        }
                self.env['finish.material'].create(fm_val)
        res = super(FinishMaterialGroup, self).write(vals)
        return res
    
    @api.multi
    def unlink(self):
        for rec in self:
            rec.move_line_id.used_in_production = False
            rec.finish_material_ids.unlink()
        return super(FinishMaterialGroup, self).unlink()
    
class FinishFinish(models.Model):
    _name = "finish.finish"
    _rec_name = "user_id"
    _description = "Finishing"
    
    product_id = fields.Many2one("goods.product", "MODEL", required="1")
    qty_product_cut = fields.Float("JUMLAH DIPOTONG", digits=(16, 0))
    qty_product_sew = fields.Float("JUMLAH DIJAHIT", digits=(16, 0))
    qty_product_done = fields.Float("JUMLAH DIPACK", digits=(16, 0))
    qty_product_reject = fields.Float("JUMLAH RUSAK", digits=(16, 0))
    user_id = fields.Many2one("res.users", "FINISHING", default=lambda self: self.env.uid)
    date = fields.Date("TANGGAL", default=fields.Date.today())
    
    @api.onchange("product_id")
    def onchange_product(self):
        if self.product_id.id:
            self._cr.execute("select sum(product_qty_cut), sum(product_qty_sew) from finish_material where goods_product_id = %s" %str(self.product_id.id))
            res = self._cr.fetchall()
            self.qty_product_cut = res[0][0]
            self.qty_product_sew = res[0][1]
    
    @api.multi
    def copy(self):
        raise UserError("Tidak boleh menggandakan data.")
    
    
    
# -*- coding: utf-8 -*-
{
    'name': "Iseng",
    
    'summary': """
        Iseng ah""",
    
    'description': """
        Challenge dari Pak Kiki
    """,
    
    'author': "Erlangga",
    'website': "https://erlaangga.github.io",
    
    # Categories can be used to filter modules in modules listing
    # Check https://github.com/odoo/odoo/blob/12.0/odoo/addons/base/data/ir_module_category_data.xml
    # for the full list
    'category': 'Tools',
    'version': '0.1',
    
    # any module necessary for this one to work correctly
    'depends': [], 
    
    # always loaded
    'data': [
        "iseng_views.xml"
    ],
}
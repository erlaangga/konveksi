# -*- coding: utf-8 -*-
{
    'name': "Auto Three Way Match",
    
    'summary': """
        Automatic three way matching""",
    
    'description': """
        When order has been received, the invoice will be generated
    """,
    
    'author': "Arkana",
    'website': "http://www.arkana.co.id",
    
    # Categories can be used to filter modules in modules listing
    # Check https://github.com/odoo/odoo/blob/12.0/odoo/addons/base/data/ir_module_category_data.xml
    # for the full list
    'category': 'Uncategorized',
    'version': '0.1',
    
    # any module necessary for this one to work correctly
    'depends': ['base', 'stock', 'purchase', 'sale', 'hr'], 
    
    # always loaded
    'data': [
        
    ],
    # only loaded in demonstration mode
    'demo': [
        'demo/demo.xml',
    ],
}
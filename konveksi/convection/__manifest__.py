# -*- coding: utf-8 -*-
{
    'name': "Convection",
    
    'summary': """
        Module for convection""",
    
    'description': """
        Module for convection
    """,
    
    'author': "Arkana",
    'website': "http://www.arkana.co.id",
    
    # Categories can be used to filter modules in modules listing
    # Check https://github.com/odoo/odoo/blob/12.0/odoo/addons/base/data/ir_module_category_data.xml
    # for the full list
    'category': 'Uncategorized',
    'version': '0.1',
    
    # any module necessary for this one to work correctly
    'depends': ['base', 'stock', 'purchase', 'sale', 'hr'], 
    
    # always loaded
    'data': [
        'security/ir.model.access.csv',
        'data/ir_sequence.xml',
        'views/product_views.xml',
        'views/stock_views.xml',
        'views/cut_material_views.xml',
        'views/finish_material_views.xml',
        'views/purchase_views.xml',
        'views/sale_views.xml',
#         'views/templates.xml',
    ],
    # only loaded in demonstration mode
    'demo': [
        'demo/demo.xml',
    ],
}
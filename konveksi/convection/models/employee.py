from odoo import models, fields

class Employee(models.Model):
    _inherit = "hr.employee"
    
    salary_line = fields.One2many("hr.salary", "employee_id", "Salary")
    debt_line = fields.One2many("hr.employee.debt", "employee_id", "Debt")

class EmployeeSalary(models.Model):
    _name = "hr.salary"
    _description = "Salary of employee"
    _rec_name = "employee_id"
    
    employee_id = fields.Many2one("hr.employee", "Karyawan")
    amount = fields.Float("Jumlah")
    salary_line_ids = fields.One2many("hr.salary.line", "salary_id", "Salary Line")
    
class EmployeeSalaryLine(models.Model):
    _name = "hr.salary.line"
    _description = "Salary line of employee"
    
    salary_id = fields.Many2one("hr.salary", "Salary", index=True)
    
class EmployeeDebt(models.Model):
    _name = "hr.employee.debt"
    _description = "Debt of employee"
    
    employee_id = fields.Many2one("hr.employee", "Karyawan")
    amount = fields.Float("Jumlah")
    date = fields.Date("Tanggal")
    
    
from odoo import api, fields, models


class Product(models.Model):
    _inherit = "product.template"
    
    is_clothing = fields.Boolean('As Clothing') 
    is_clothing_material = fields.Boolean('As Clothing Material')
    clothing_material_line = fields.One2many("product.template", "clothing_id", string="Clothing Materials")
    clothing_id = fields.Many2one('product.template', 'Material Clothing of', domain="[('is_clothing', '=', True])")
#     qty_produce = fields.Float('Quantity for Clothing')
    
    
# -*- coding: utf-8 -*-

from odoo import models, fields, api


class CutMaterial(models.Model):
    _name = "cut.material"
    _description = "Material Cutting"
    
    @api.model
    def _default_employee_id(self):
        return self.env['hr.employee'].search([('user_id', '=', self.env.uid)], limit=1).id
    
    name = fields.Char("Name")
    user_id = fields.Many2one("res.users", "User", compute="_get_user_employee", readonly=True, store=True)
    employee_id = fields.Many2one("hr.employee", "Pemotong", default=_default_employee_id, domain="[('user_id', '!=', user_id)]")
    state = fields.Selection([('draft', 'Draft'), ('confirmed', 'Confirmed'), (
        'cutting', 'Cutting'), ('check', 'Checking'), ('done', 'Done'), ('cancel', 'Batal')], 'State', default="draft")
    product_id = fields.Many2one('product.product', 'Pakaian', domain="[('is_clothing', '=', True)]")
    product_qty = fields.Float('Clothing Quantity')
    finish_material_ids = fields.One2many(
        'finish.material', 'cut_id', 'Finish Materials', copy=True)
    date = fields.Datetime("Date", default=fields.Datetime.now())
    product_material_ids= fields.One2many("product.product", string="Material", compute="_compute_product_material")
    
    @api.depends("employee_id")
    def _get_user_employee(self):
        for rec in self:
            rec.user_id = rec.employee_id.user_id
    
    @api.depends("product_id")
    def _compute_product_material(self):
        for rec in self:
            material_product = self.env['product.product'].search([('product_tmpl_id', 'in', rec.product_id.clothing_material_line.ids)])
            rec.product_material_ids = material_product.ids
        
    @api.multi
    def act_confirm(self):
        now = fields.Datetime.now()
        for line in self.finish_material_ids:
            if not line.date:
                line.date = now
                line.act_check()
            move_data = self._prepare_move_data(line)
            move = self.env['stock.move'].create(move_data)
            line.move_id = move.id
            move._action_assign()
        self.state = 'confirmed'

    @api.multi
    def act_cut(self):
        self.state = 'cutting'

    @api.multi
    def act_check(self):
        self.state = 'check'

    @api.multi
    def act_done(self):
        self.state = 'done'

    @api.multi
    def act_cancel(self):
        self.state = 'cancel'

    @api.multi
    def act_draft(self):
        self.state = 'draft'

    @api.model
    def create(self, vals):
        name = self.env["ir.sequence"].next_by_code("cut.material")
        vals.update({'name': name})
        return super(CutMaterial, self).create(vals)
    
    @api.multi
    def _prepare_move_data(self, line):
        move_line = line.move_line_id
        move_id = line.move_line_id.move_id
        picking_type_id = self.env['stock.picking.type'].search([('code', '=', 'internal')], limit=1)
        location_id = self.env['stock.location'].search([('usage', '=', 'production')], limit=1)
        template = {
            'name': self.name or '',
            'product_id': move_line.product_id.id,
            'product_uom': move_line.product_id.uom_id.id,
            'product_uom_qty': move_line.product_uom_qty,
            'date': line.date,
            'date_expected': line.date,
            'location_id': move_line.location_dest_id.id,
            'location_dest_id': location_id.id,
            'partner_id': line.user_id.partner_id.id,
            'move_orig_ids': [(6, 0, move_id.ids)],
            'state': 'draft',
            'purchase_line_id': move_id.purchase_line_id.id,
            'company_id': move_id.company_id.id,
            'price_unit': move_id.purchase_line_id.price_unit,
            'picking_type_id': picking_type_id.id,
            # 'group_id': self.order_id.group_id.id,
            'origin': self.name,
            # 'route_ids': self.order_id.picking_type_id.warehouse_id and [(6, 0, [x.id for x in self.order_id.picking_type_id.warehouse_id.route_ids])] or [],
            'warehouse_id': move_id.picking_type_id.warehouse_id.id,
            # 'procure_method': '',
        }
        return template
    
class FinishMaterial(models.Model):
    _name = "finish.material"
    _description = "Material Finishing"
    
    name = fields.Char("Name")
    user_id = fields.Many2one("res.users", "Assigner")
    sew_user_id = fields.Many2one("res.users", "Sewer")
    finish_user_id = fields.Many2one("res.users", "Checker")
    state = fields.Selection([('draft', 'Draft'), ('sew', 'Sewing'),
                              ('check', 'check'), ('finish', 'Finish')], 'State', default="draft")
    date = fields.Datetime("Date", default=fields.Datetime.now())
    date_finish = fields.Datetime("Finish Date")
    product_id = fields.Many2one('product.template', 'Bahan')
    move_line_id = fields.Many2one('stock.move.line', 'Bahan')
    move_id = fields.Many2one('stock.move', 'Move')
    product_qty = fields.Float("Quantity", related="move_line_id.qty_done")
    product_qty_cut = fields.Float("Jumlah Potongan")
    cut_id = fields.Many2one('cut.material', 'Cutting')
    
    @api.multi
    def act_check(self):
        self.state = "check"

    @api.multi
    def act_sew(self):
        self.state = 'sew'
    
    @api.multi
    def act_finish(self):
        self.state = 'finish'
    
    @api.model
    def create(self, vals):
        name = self.env["ir.sequence"].next_by_code("finish.material")
        vals.update({'name': name})
        return super(FinishMaterial, self).create(vals)
    
    
class FinishMaterialLine(models.Model):
    _name = "finish.material.line"
    _description = "Material Finishing Detail"
    
    finish_id = fields.Many2one("finish.material", "Finish")
    product_id = fields.Many2one("product.product", "Baju")
    employee_id = fields.Many2one("hr.employee", "Employee")
    
    
    @api.multi
    def act_take(self):
        for rec in self:
            rec.user_id = self.env.uid
# class convection(models.Model):
#     _name = 'convection.convection'

#     name = fields.Char()
#     value = fields.Integer()
#     value2 = fields.Float(compute="_value_pc", store=True)
#     description = fields.Text()
#
#     @api.depends('value')
#     def _value_pc(self):
#         self.value2 = float(self.value) / 100
